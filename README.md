# Unbiased

A tool to read from a JSON list of cognitive biases, and email one to a specific email address at a specific interval (with an option for some randomness).

__WARNING:__ May turn you in to a knob who constantly calls out cognitive biases.

## Setup
- Rename and edit config.cfg.sample to config.cfg with your SMTP settings.
- Run `unbiased.py` as a cron - each time it will email you a cognitive bias.
- Edit `list-of-cognitive-biases.json` as you see fit.
