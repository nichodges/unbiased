# -*- coding: utf-8 -*-
"""
Unbiased
========
Loads JSON of cognitive biases, selects a random one and emails it to
email address(es) specified in config.
"""
import os
from ConfigParser import SafeConfigParser
import codecs
import json
import random
import smtplib
from email.mime.text import MIMEText

os.chdir(os.path.dirname(os.path.abspath(__file__)))

parser = SafeConfigParser()
with codecs.open('config.cfg', 'r', encoding='utf-8') as f:
    parser.readfp(f)

# Config to constants
JSON_FILE = parser.get('unbiased', 'json_file')
JSON_SCHEMA = parser.get('unbiased', 'json_schema').split(",")
JSON_OBJECT = parser.get('unbiased', 'json_object')
EMAIL_SERVER = parser.get('email', 'server')
EMAIL_PORT = int(parser.get('email', 'port'))
EMAIL_USERNAME = parser.get('email', 'username')
EMAIL_PASSWORD = parser.get('email', 'password')
EMAIL_TO = parser.get('unbiased', 'to_email')
EMAIL_FROM = parser.get('unbiased', 'from_email')

# Load our JSON and grab a random record (ran)
json_data = open(JSON_FILE)
data = json.load(json_data)[JSON_OBJECT]
ran = data[random.randint(0,(len(data)-1))]

# Send an email
msg = MIMEText("<a href='%s'><b>%s</b></a><br>%s<br><br>" % \
            (ran[JSON_SCHEMA[2]], ran[JSON_SCHEMA[0]], ran[JSON_SCHEMA[1]]), \
            'html')
msg['Subject'] = "Your daily cognitive bias"
msg['From'] = EMAIL_FROM
msg['To'] = EMAIL_TO

s = smtplib.SMTP_SSL(EMAIL_SERVER, EMAIL_PORT)
s.login(EMAIL_USERNAME, EMAIL_PASSWORD)
s.sendmail(EMAIL_FROM, [EMAIL_TO], msg.as_string())
s.quit()